import adcarray as adc
import pylab as pl
import numpy as np
import o32reader as rdr



filename='DATA/0195'

#Create rawreader class variable
r=rdr.o32reader(filename)

#Create adcarray class variable
a=adc.adcarray()

c=0


#Create arrays for saving values
s1=np.zeros((12,144))
s2=np.zeros((12,144))


n=10
#Cycle through events in file
for raw in r:
	c+=1
	a.analyse_event(raw)
	
	s1+=np.sum(a.data,axis=2)		#Mean
	s2+=np.sum(np.square(a.data),axis=2)	#Mean squared

#	if c==n:
#		break
	

#Calculate standard deviation
Q=np.sqrt((s2/(30*c))-np.square(s1/(30*c)))


#Plot results
pl.imshow(Q,vmin=0,vmax=3,aspect='auto',interpolation='nearest',origin='lower')
pl.title('RMS noise map:'+filename[5:9])
pl.xlabel('Pad column')
pl.ylabel('Pad row')
pl.colorbar()

pl.show()


