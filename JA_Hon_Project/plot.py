import ADCarray as adc

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

x = np.arange(0,8,1)
y = np.arange(0,12,1)

xs, ys = np.meshgrid(x, y)

# z = calculate_R(xs, ys)
zs = adc.a.adc_ave


fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface(xs, ys, zs, rstride=1, cstride=1, cmap='hot')
plt.show()
