import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import adcarray as adc
import numpy as np
import o32reader as rdr


#Load 3D data cube

data=np.load('DATA/Cosmics/cosmic_0134_4353.npy')

#Choose the coordinates on the TRD of the cosmic

xmin=60
xmax=70

ymin=8
ymax=12


# Define the figure
fig = plt.figure()
i=0



#Defining the image of animation
def an(i):
	image = data[ymin:ymax,xmin:xmax,i]
	return image


#Cosmic image is saved
im = plt.imshow(an(i), animated=True,aspect='auto',origin='lower')


#Function for cosmic image update
def updatefig2(*args):
	global i
	i+=1  
	im.set_array(an(i))

	return im,


#Animation is displayed
def show_animation():
	ani = animation.FuncAnimation(fig, updatefig2, interval=150,frames=25,repeat=False, blit=True)
	plt.show()
	global i
	i=0


show_animation()


