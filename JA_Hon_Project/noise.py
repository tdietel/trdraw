import adcarray as adc
import pylab as pl
import numpy as np
import o32reader as rdr


#Imports file name
filename='DATA/0195'


#Creates adcarray object variables
a=adc.adcarray()


#Load the file
a.data=np.load('DATA/mean_data_'+filename[5:9]+'.npy')

print(np.sum(a.data)/(30*144*12)) 	#Display average ADC value

#Calculate mean over time bins, and save in first timebin position
for i in range(0,12):
	for j in range(0,144):
		a.data[i,j,0]=np.mean(a.data[i,j,:])


#Plotting the complete mean TRD
pl.title('Average over timebins')
pl.imshow(a.data[:,:,0],aspect='auto',origin='lower')
pl.title('Mean ADC values map:'+filename[5:9])
pl.xlabel('Pad column')
pl.ylabel('Pad row')
pl.colorbar()
pl.show()

fig, ax = pl.subplots()


#Dividing the 3D cubes into the ROB dimensions

pl.subplot(3,2,1)
pl.title('ROB 5')
pl.grid()
pl.imshow(a.data[8:12,0:72,0],aspect='auto',interpolation='nearest',origin='lower',extent=[0,4,8,12])
pl.colorbar()


pl.subplot(3,2,2)
pl.title('ROB 4')
pl.grid()
fig = pl.imshow(a.data[8:12,73:143,0],aspect='auto',interpolation='nearest',origin='lower',extent=[4,8,8,12])
pl.colorbar()


pl.subplot(3,2,3)
pl.grid()
pl.title('ROB 3')
pl.imshow(a.data[4:8,0:72,0],aspect='auto',interpolation='nearest',origin='lower',extent=[0,4,4,8])
pl.colorbar()

pl.subplot(3,2,4)
pl.grid()
pl.title('ROB 2')
pl.imshow(a.data[4:8,73:143,0],aspect='auto',interpolation='nearest',origin='lower',extent=[4,8,4,8])
pl.colorbar()

pl.subplot(3,2,5)
pl.grid()
pl.title('ROB 1')
pl.imshow(a.data[0:4,0:72,0],aspect='auto',interpolation='nearest',origin='lower',extent=[0,4,0,4])
pl.colorbar()

pl.subplot(3,2,6)
pl.grid()
pl.title('ROB 0')
pl.imshow(a.data[0:4,73:143,0],aspect='auto',interpolation='nearest',origin='lower',extent=[4,8,0,4])
pl.colorbar()
pl.show()

