#!/usr/bin/env python3


import adcarray as adc
import pylab as pl
import numpy as np

import o32reader as rdr

import argparse


parser = argparse.ArgumentParser(description='Display an event.')
parser.add_argument('run', type=int, help='run number')
parser.add_argument('event', type=int, nargs='?', help='event number')

args = parser.parse_args()

r=rdr.o32reader('/data/raw/0134')


a=adc.adcarray()

bins=np.linspace(0,1000,1000)
hdata,bla = np.histogram((),bins)

evno = 0

rows = np.linspace(0,12,12,False)
cols = np.linspace(0,144,144,False)

    
for raw in r:

    evno += 1

    a.analyse_event(raw)

    if args.event is not None and args.event < evno:
        continue
        
    
    tbsum = np.sum(a.data[0:3,:,:], axis=2)

    cand = np.argwhere(np.logical_and(tbsum>400, tbsum<5000))

    if len(cand) == 0:
        continue
    
    print (cand)

    

    
    pl.figure(figsize=(12,13))
    pl.subplots_adjust(hspace=.2)


    for i in range(12):
        pl.subplot(12,1,i+1)
        #print (a.data[i,:,:])
        pl.matshow(np.transpose(a.data[i,:,:]), fignum=False,aspect=0.3)

        
    pl.show()
        
