
import numpy
import re
from datetime import datetime

class rawreader:

    #Initial state variables:
    def __init__(self,file_name):

        self.infile=open(file_name, 'r')
        self.line_number=0
        self.linebuf = None
        self.HC_header=0

        self.ev_timestamp = None
        self.blk_sfp = None
        self.blk_size = None
        self.blk_unread = 0
        
    def read_line(self):
        if self.linebuf is not None:
            tmp = self.linebuf
            self.linebuf = None
            return tmp
        else:
            self.line_number+=1
            self.lastline = self.infile.readline().rstrip()
            return self.lastline
        
    def peek_line(self):
        self.line_number+=1
        self.linebuf = self.infile.readline().rstrip()
        return self.linebuf
    
    def get_dword(self):
        return numpy.uint32(int(self.read_line()))
    
    def next_event(self):

        #while self.next_block():
        #    print ("skipping data block sfp[%d], %d bytes",
        #           (self.blk_sfp, blk_size))
        #    pass
        
        if self.read_line() != '# EVENT':
            print ("Event not found: '%s'", self.lastline)
            return False
                
        m = re.search('# *time stamp: *(.*)',
                      self.read_line())

        self.ev_timestamp = datetime.strptime( m.group(1),
                                               '%Y-%m-%dT%H:%M:%S.%f')

        return True
        
    def next_block(self):

        self.infile.seek( 4 * self.blk_unread )
        
        if self.read_line() != '## DATA SEGMENT':
            return False

        m = re.match('## *sfp *(.*)', self.read_line())
        self.blk_sfp = int(m.group(1))

        m = re.search('## *size: *(.*)', self.read_line())
        self.blk_size = int(m.group(1))
        self.blk_unread = blk.size
        return True

        
        #        NB=0
#        
#        while NB<=2:
#	    line=self.get_dword()
#            
#	    #Count current event
#	    if line.rfind('## New Block')>=0:
#		NB+=1
#                
#                
#	    #Stop loop when next event starts
#	    
#	    if NB==2:
#                
#		#Return to start of following event
#		self.infile.seek(self.infile.tell()-len(line),0)
#		break
#            
            
    #Reset the file.
    def reset(self):
                
        self.infile=open(self.infile.name)
                
                
