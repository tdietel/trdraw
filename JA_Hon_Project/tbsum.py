#!/usr/bin/env python3

import adcarray as adc
import pylab as pl
import numpy as np

import o32reader as rdr

import argparse

parser = argparse.ArgumentParser(description='Search for possible cosmics.')
parser.add_argument('run', type=int, help='run number')

args = parser.parse_args()

r=rdr.o32reader('/data/raw/%04d' % args.run)

a=adc.adcarray()

bins=np.linspace(1,20000,2000)
hdata,bla = np.histogram((),bins)

adcbins=np.linspace(-0.5,1023.5,1024)
adchdata,blub = np.histogram((),adcbins)


evno = 0

for raw in r:
    evno += 1
    print("event %d" % evno)

    
    
    if (args.run==367 and evno==48): continue
    if (args.run==367 and evno==49): continue

    if (args.run==360 and evno==135):
        break
    
    for blk in raw['datablocks']:
        print ("   blk: size=%d" % (blk['size']) )
        
    try:
        a.analyse_event(raw)
    #print (a.data)

    except adc.datafmt_error:
        print ("error processing event")
        continue

    tbsum = np.sum(a.data[:,:,:], axis=2)

    foo,bar = np.histogram(tbsum.flatten(),bins)
    hdata += foo

    adchdata += np.histogram(a.data.flatten(),adcbins)[0]
    
    if (evno>200):
        break
        
pl.bar(bins[:-1],hdata, log=True)

pl.figure("ADC spectrum")
pl.bar(adcbins[:-1],adchdata, log=True)

pl.show()
