#!/usr/bin/env python3


#import ADCarray as adc
import pylab as pl
import numpy as np

import o32reader as rdr
import adcarray as adc


r=rdr.o32reader('/data/raw/0238')
a=adc.adcarray()

s1 = np.zeros((12,144))
s2 = np.zeros((12,144))

nev = 0

for raw in r:
    nev += 1
    s1 += np.sum(a.data, axis=2)
    s2 += np.sum(np.square(a.data), axis=2)

    if nev%20 == 0:
        print ("%8d events processed" % nev)

    if nev >= 30:
        break

pl.matshow(s1,aspect='auto')
pl.colorbar()
pl.show()
