import Rawreader as rdr
import ADCarray as ADC
import os
import numpy as np
import pylab as pl

filenames=os.listdir('Data')

print(filenames)


#filename='Data/0051'
#filename='Data/0053'

Z=[[],[]]

for i in filenames:
	filename=str('Data/') + i

	R=rdr.rawreader(filename)
	A=ADC.adcarray(filename)

	time_bins,values=A.read_file(R)

	Z[1].append(values)
	Z[0].append(i)


for i in range(len(Z[0])):
	print('file: ',i+1)

	for j in range(len(Z[1][i])):
		print('Event: ',j+1)

		for k in range(len(Z[1][i][j])):
			print('time stamp: ',k)
			print(Z[1][i][j][k])
		
