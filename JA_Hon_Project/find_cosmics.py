#!/usr/bin/env python3


import adcarray as adc
import pylab as pl
import numpy as np

import o32reader as rdr

import argparse


parser = argparse.ArgumentParser(description='Search for possible cosmics.')
parser.add_argument('run', type=int, help='run number')

args = parser.parse_args()

r=rdr.o32reader('/data/raw/%04d' % args.run)


a=adc.adcarray()

bins=np.linspace(0,1000,1000)
hdata,bla = np.histogram((),bins)

evno = 0

rows = np.linspace(0,12,12,False)
cols = np.linspace(0,144,144,False)

    
for raw in r:

    evno += 1
    print ('.', end='', flush=True)
    
    try:
        a.analyse_event(raw)

    except adc.datafmt_error:
        print ("error processing event")
        continue

    
    #tbsum = np.sum(a.data[8:10,:,:], axis=2)
    tbsum = np.sum(a.data[:,:,:], axis=2)

    cand = np.argwhere(np.logical_and(tbsum>400, tbsum<10000))

    if len(cand) == 0:
        continue

    print ("\n\nrun %04d event %d" % (args.run, evno))

    for ca in cand:
        row = ca[0]
        col = ca[1]

        if (row==8 or row==9):
            print ("  skip row %2d col %3d - noisy region" % (row,col))
            continue

        mincol = col-5 if col>=5 else 0
        maxcol = col+5 if col<=139 else 144
        
        print ("  row %2d col %3d - tbsum=%d" % (row,col,tbsum[row,col]))

        #print (a.data[row,:,:])
        
        pl.matshow(np.transpose(a.data[row,mincol:maxcol,:]))
        pl.show()

    print ("\n")
