import numpy as np
import matplotlib.pyplot as pl
import scipy.optimize as optimize
import math
import adcarray as adc
import numpy as np
import o32reader as rdr

#Load specific event
data=np.load('DATA/Cosmics/cosmic_0134_4353.npy')

#Subtract average ADC value from non-zero counts
for x in range(144):
	for y in range(12):
		for tb in range(30):
			if data[y,x,tb] != 0:
				data[y,x,tb]=data[y,x,tb]-9.62

#Zoom image to pads that are triggered by cosmic
xmin=60
xmax=70

ymin=8
ymax=11

#Extract cosmic ray in flattened array:
cosmic=[]
for i in range(ymin,ymax):
	for j in range(xmin,xmax):
		cosmic.append(data[i,j,:])

#Calculating the centre of mass in x/y components

comx=[]
comy=[]
com=np.array([])

#Looping through time bins
for tb in range(30):
	moment=0
	mass=0

	#For the x-component, we average over y.
	for i in range(ymin,ymax):
		for j in range(xmin,xmax):
			r=np.array([j,i])
		
			moment+=r*data[i,j,tb]
			mass+=data[i,j,tb]

	X,Y=moment/mass
	comx.append(X)
	comy.append(Y)
	#com=np.append(com,[X,Y])


	


#Fit straight lines through the centre of mass functions
tb=np.arange(0,30)

#Residuals funtion determines the slope and constant of the line
def residuals(p,yy,xx):
	m,c=p
	err=abs(yy-(m*xx+c))
	return err

#Initial guess
p0=np.array([4,0])

#Perform calculation
leastfit=optimize.leastsq(residuals,p0,(comx,tb))
leastfit2=optimize.leastsq(residuals,p0,(comy,tb))

#Extract coefficients
coeffs=leastfit[0]
coeffs2=leastfit2[0]

#Draw lines
fit=coeffs[0]*tb+coeffs[1]
fit2=coeffs2[0]*tb+coeffs2[1]

#Calculate inclination in degrees from slope
angle=math.atan(coeffs[0])
angle=angle*(180/np.pi)

angle2=math.atan(coeffs2[0])
angle2=angle2*(180/np.pi)


#Plot the results

#First plot the average over time bin, for each triggered pad
pl.subplot(2,1,1)

pl.title('Cosmic Detected-(mean over timebin)')
tbsum=np.sum(data[ymin:ymax,xmin:xmax],axis=2)
pl.imshow(tbsum/30,aspect='auto',interpolation='nearest',origin='lower',vmin=10)
pl.colorbar()

#Plot flattened array of cosmic, over time bin
#The pad count in y-axis, are numbered left to right, top to bottom, form the 2D plane
pl.subplot(2,1,2)

pl.title('Cosmic Spread out over time bins')
pl.imshow(cosmic,aspect='auto',interpolation='nearest',origin='lower',vmin=10)
pl.xlabel('timebin')
pl.ylabel('pixel spread')
pl.colorbar()

pl.show()



#Plot the centre of mass over time bin, with straight line fit

pl.subplot(2,1,1)
pl.title('Column-Trajectory')
pl.plot(comx,'*')
pl.plot(tb,fit)
pl.ylabel('Column')
pl.xlabel('Time bin')
pl.grid()

pl.subplot(2,1,2)
pl.title('Row-Trajectory')
pl.plot(comy,'*')
pl.plot(tb,fit2)
pl.ylabel('Row')
pl.xlabel('Time bin')
pl.grid()
pl.show()


##########################################################################################3

#3D Analysis

#The data above is used to derive a 3D vetor of the trajectory of the cosmic ray
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d


#Defining the ave vector
tb = np.linspace(0,29, 30)
x = coeffs[0]*tb+coeffs[1]		
y = coeffs2[0]*tb+coeffs2[1]


#Converting to spherical coorinates

#Assume the unit vector is in cm.

dz=30*(3/20)		#Assume 20 time bins = 3 cm

dx=coeffs[0]*30*0.77	#The column resolution is 0.77 cm

dy=coeffs2[0]*30*8.91	#The row resolution is 8.91cm

dr=np.sqrt(dx**2 + dy**2 + dz**2)



#Keep angles poitive
dtheta=math.acos(dz/dr)
if dtheta < 0:
	dtheta=2*np.pi + dtheta

dphi=math.atan(dy/dx)
if dphi < 0:
	dphi=2*np.pi + dphi


#Convert to degrees
dtheta=dtheta*180/np.pi
dphi=dphi*180/np.pi


print(dx,' ',dy,' ',dz)
print(dtheta,' ',dphi)


#Print spherical angles:
print('Cosmic direction')
print('Angle to normal: ',round(dtheta,2))
print('XY plane angle: ',round(dphi,2))



#Choosing plot properties:
fig = plt.figure()
ax = fig.gca(projection='3d')

#Actual trajectory
ax.plot(comx, comy, tb, 
        label = 'Actual', # label of the curve
        color = 'Red',      # colour of the curve
        linewidth = 5,            # thickness of the line
        linestyle = '-'            # available styles - -- -. :
        )

#Average trajectory
ax.plot(x, y, tb, 
        label = 'Fit', # label of the curve
        color = 'Blue',      # colour of the curve
        linewidth = 2,            # thickness of the line
        linestyle = '-'            # available styles - -- -. :
        )

rcParams['legend.fontsize'] = 11    # legend font size
ax.legend() 


ax.set_xlabel('col-deg: '+str(round(angle,2)),fontsize=15)
ax.set_xlim(xmin,xmax)
ax.set_ylabel('row-deg: '+str(round(angle2,2)),fontsize=15)
ax.set_ylim(ymin, ymax)
ax.set_zlabel('Time Bin')
ax.set_zlim(29, 0)


ax.set_title('Cosmic Ray')

ax.view_init(elev=18, azim=-27)             # camera elevation and angle 
ax.dist=9                                   # camera distance
plt.show() 


