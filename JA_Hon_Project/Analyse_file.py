import Rawreader as rdr
import ADCarray as ADC


filename='Data/0102'
#filename='Data/both'

#filename='Data/0053'
#filename='Data/test'



R=rdr.rawreader(filename)
A=ADC.adcarray(filename)

#read_file is function of object(rawreader)

time_bins,values=A.read_file(R)

#values: value[event number][time_bin][x/coordinate][y/coordinate]
#time_bins: time_bin[event_number][bin_number]



