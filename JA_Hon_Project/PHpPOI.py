#!/usr/bin/env python3

import defaults
import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib.pyplot as plt
import itertools
from sys import argv


file = argv[1]
reader = rdr.o32reader(file)
analyser = adc.adcarray()
tbsum_mask = 320

sumtrkl = np.zeros(30)
ntrkl = 0
    


for evno, raw_data in enumerate(reader):
    # if evno % defaults.PRINT_EVNO_EVERY == 0:
    #     print("Proccessing events %d–%d"
    #             % (evno, evno + defaults.PRINT_EVNO_EVERY))

    #if evno >= 20: break
    if evno >= 1000: break

    if evno != 0:
        print('EVENT', evno)

        try:
            analyser.analyse_event(raw_data)
        except adc.datafmt_error as e:
            continue

        data = analyser.data[:12]  # The last four rows are zeros.
        data[defaults.DATA_EXCLUDE_MASK] = 0
        tbsum = np.sum(data, 2)
        # print(tbsum)

        tbsum_zeroes = tbsum < tbsum_mask
        tbsum[tbsum_zeroes] = 0

        point_of_interest = np.argwhere(tbsum > 350)
        if len(point_of_interest) == 0: continue

        #print(point_of_interest)

        for r in sorted(set(point_of_interest[:,0])):
            pads = [x[1] for x in point_of_interest if x[0]==r]

            print ( "  Row ", r, ": ", pads)

            start = False
            current = False
            for p in pads+[999]:
                if not start:
                    start=p
                    current=p-1

                if p==(current+1):
                    current = p
                else:
                    npad = current-start+1
                    trkl = data[r, start:current+1, :]-9.5

                    print ("    Tracklet: ", start, current, np.sum(trkl))

                    if ( np.sum(trkl[:,0:6]) > 50 ):
                        continue
                    
                    #print( trkl )

                    #print( "sum: ", np.sum(trkl, 0) )
                    sumtrkl += np.sum(trkl, 0)
                    ntrkl += 1
                    plt.plot(np.sum(trkl,0))
                    
#        for p in point_of_interest:
#            if p[1] == 0:
#                if tbsum[p[0],p[1]-1] > tbsum[p[0],p[1]]: continue
#                # if tbsum[p[0],p[1] + 1] > tbsum[p[0],p[1]]:
#            if p[1] == 144:
#                # for i in range(4):
#                if tbsum[p[0], p[1] - 1] > tbsum[p[0], p[1]]: continue
#                # if tbsum[p[0], p[1] + 1] > tbsum[p[0], p[1]]: continue
#            else:
#                # for i in range(4):
#                if tbsum[p[0], p[1] - 1] > tbsum[p[0],p[1]]: continue
#                if tbsum[p[0], p[1] + 1] > tbsum[p[0], p[1]]: continue
#
                # else: continue

            print(p)
            #plt.imshow(data[p[0], :, :].T)
            #plt.plot( [p[1]], [31], "ro")
            #plt.show()
            

        #plt.imshow(tbsum, cmap='hot', vmin=tbsum_mask - 1, norm=None, extent=None, aspect=None,
        #           interpolation=None)
        #plt.colorbar(orientation='horizontal')
        # plt.show()


        # print(point_of_interest)
        # for tbsum_coordinates, info in enumerate(point_of_interest):
        #     padrow = info[0]
        #     padrowlist.append(padrow)
        #     pad = info[1]
        #     padlist.append(pad)
        # print('pad row ', padrow, 'pad ', pad, 'reading ', tbsum_value)
        # plt.imshow(tbsum)
        #plt.plot(point_of_interest[:, 1], point_of_interest[:, 0], 'go')
        #plt.show()

    else:
        continue



    
# intensity_sum = sum(intensity)
# plt.imshow(intensity_sum)
# plt.imshow(intensity_sum, cmap='hot', vmin=tbsum_mask - 1, norm=None, extent=None, aspect=None, interpolation=None)
# plt.colorbar(orientation='horizontal')

plt.figure()
plt.plot(sumtrkl/ntrkl)
plt.show()
