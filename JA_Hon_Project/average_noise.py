import adcarray as adc
import pylab as pl
import numpy as np

import o32reader as rdr

filename='DATA/0134'

r=rdr.o32reader(filename)


a=adc.adcarray()
mean=np.zeros((12,144,30))

c=0


for raw in r:
	c+=1
	a.analyse_event(raw)
	mean+=a.data


mean=mean/c

np.save('DATA/mean_data_'+filename[5:9],mean)

pl.title('Average')
pl.imshow(mean[:,:,0],aspect='auto')
pl.colorbar()
pl.show()

