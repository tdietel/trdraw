from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np


tb = np.linspace(0,29, 30)
x = 0.5*tb+16
y = 0.5*tb+8


fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(x, y, tb, 
        label = 'Parametric Curve', # label of the curve
        color = 'DarkMagenta',      # colour of the curve
        linewidth = 3.2,            # thickness of the line
        linestyle = '-'            # available styles - -- -. :
        )
rcParams['legend.fontsize'] = 11    # legend font size
ax.legend() 


ax.set_xlabel('X axis')
ax.set_xlim(0, 144)
ax.set_ylabel('Y axis')
ax.set_ylim(0, 12)
ax.set_zlabel('Z axis')
ax.set_zlim(0, 29)


ax.set_title('3D line plot,\n parametric curve', va='bottom')

ax.view_init(elev=18, azim=-27)             # camera elevation and angle 
ax.dist=9                                   # camera distance
plt.show() 
