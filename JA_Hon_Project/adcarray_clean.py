import numpy as np
import math
import pylab as pl


class adcarray:

    end_of_tracklet = int('0x10001000',0)

#Defining the initial variables for class    
    def __init__(self):
        self.data=np.zeros((12,144,30))
        self.clean_data=np.zeros((12,144,30))

        self.HC_header=0
        self.HC1_header=0
        self.ntb=30
        self.MCM_header=0

        self.line=-1
        self.sfp=0




    def get_dword(self,dic):
        self.line+=1
        return dic['datablocks'][self.sfp]['raw'][self.line]



    def read_hc_header(self,dic):
        '''
        Parameters: rdr = Rawreader variable

        Function reads all Half chamber headers
        '''

        self.HC_header=self.get_dword(dic)
        print('HC_header: ',self.HC_header)

            
        hw=(int(hex(self.HC_header)[0:10],0) >> 14) & 0x7

        #Read additional header words
        if hw >= 1:
            self.HC1_header=self.get_dword(dic)
            print('HC1_header: ',self.HC1_header)

            self.ntb=(int(hex(self.HC1_header)[0:10],0)>>26)&0x3F
            for i in range(1,hw):
                check=self.get_dword(dic)

                






    def extract_mcm_data(self,dic):

        ''' 
        Parameter:rdr = Rawreader variable

        Extracts MCM header and all the mcm data that follows under the header
        '''

        self.MCM_header=self.get_dword(dic)
    
    #Cycle throught 21 channels per mcm
        for ch in range(0,21):
    
        #Cycle through number of words (timebin) associated with the mcm
            for i in range(0,adcarray.N_words(self.ntb)):
                dword=self.get_dword(dic)
                tb=i*3
                
                
                if ch>=0 and ch < 18:
                #Extract and save data in 3D array
                    self.read_dword(dword,tb,ch)












    def analyse_event(self,dic):
        '''
        Parameter: rdr = Rawreader variable
        Extracts all the data from event in file and reads into 3D data cube: self.data
        '''

        for sfp in [0,1]:
            self.sfp=sfp
            self.line=-1
            c=0
            while c!=2:
                line=self.get_dword(dic)
#                print(hex(line))

                if line == adcarray.end_of_tracklet:
                    c+=1


        #Read HC_header:

            self.read_hc_header(dic)


        #Cycle

            for rob in range(0,3):
                #Cycle through mcm's on read_ou_board
                for mcm in range(0,16):
                    self.extract_mcm_data(dic)

#            print(self.line)










    def read_dword(self,dword,tb,ch):
        '''
        Extract information from dword, and read it into the 3D data cube
        Parameters:
                dword=MCM_data dword
                tb=time_bin the dword is associated with
                ch=channel from where the data comes from
        writes: self.data 3D cube
        '''
        #Find position
        col,row=self.pos()

        #print(col,row)

        #from dword, write into self.data
        self.data[row][col*18+ch][tb+2]=(dword>>22)& 0x3ff
        self.data[row][col*18+ch][tb+1]=(dword>>12)& 0x3ff
        self.data[row][col*18+ch][tb]=(dword>>2)& 0x3ff


#        print(self.pos(),col*18+ch)





    def get_adc(self,row,col,ch,tb):

        '''
        Works on a 12x144xtb 3D data cube:

                row: dimension 0-11 (y-direction of adc)
                col: dimension 0-7 (x-direction of adc)
                ch: channel number of adc
                tb:  # of timebin

        returns value in data cube
        '''

        return self.data[row][col*18+ch][tb]


#Extract position form MCM_header
    def pos_ex(self):

        '''
        Extracts read_out_board and adc position from mcm_header
        returns: read_out_board,MCM_position
        '''

        ROB=(int(hex(self.MCM_header),0)>>28) & 0x7
        MCMpos=(int(hex(self.MCM_header),0)>>24) & 0xF
        return ROB,MCMpos

#X-coordinate
    def xc(self,ROB,MCMpos):

        '''
        Parameters: ROB = read_out_board
                MCMpos = MCM_position

        Returns x-coordinate of position on the trd board
        '''

        return 7-(4*(ROB%2) + MCMpos%4)

#Y-coordinate
    def yc(self,ROB,MCMpos):

        '''
        Parameters: ROB = read_out_board
                MCMpos = MCM_position

        Returns y-coordinate of position on the trd board
        '''
        
        return 4*(math.floor(ROB/2)) + math.floor(MCMpos/4)


#Covert Readout board and ADC to X/Y-pos
    def conv(self,ROB,MCMpos):
        '''
        Converts read_out_board and mcm to x and y positions
        Parameters:        
                    ROB=Read_ot_board
                    MCMpos=mcm position

        returns: x,y position
        '''
        col=self.xc(ROB,MCMpos)
        row=self.yc(ROB,MCMpos)
        
        return col,row    #(x,y)

#Find position on board
    def pos(self):
        '''
        Finds x,y position from MCM_header
        returns x,y coordinates
        '''
        ROB,MCMpos = self.pos_ex()
        #x,y = self.conv(ROB,MCMpos)
        #print("%02d:%02d -> %02d/%03d     MCM hdr: %08x" % (ROB,MCMpos,x,y,self.MCM_header))
        return self.conv(ROB,MCMpos)

#Find Number of words from Number of timebins
    def N_words(Nt):
        '''
        Calculates the number of dwords, given the number of time_bins
        Parameter: Nt = Number of time_bins
        '''
        return math.floor((Nt-1)/3) + 1




    def reset(self):
        '''
        Resets initial varaiables associated with the adcarray data
        '''

        self.data=np.zeros((12,144,30))

        self.HC_header=0
        self.HC1_header=0
        self.ntb=0
        self.MCM_header=0


