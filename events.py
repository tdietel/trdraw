def count_events(file_name):
    event_count = 0
    r = open(file_name)
    for line in r:
        if line.rstrip().find('## New Event') == -1:
            continue
        event_count += 1
    return event_count


def find_event(Half_chamber_file,event_nr,words):
    event = 0
    started = False
    with open(Half_chamber_file, "r") as fp:
        for i, line in enumerate(fp.readlines()):
            if event == event_nr:
                started = True
                words.append(line.rstrip())
            if line.rstrip() == "## New Event":
                event += 1
            if started == True and line.rstrip() == "0x00000000": #Potentially loose information
                break
    return words

def read_events(File_1, File_2, event_nr):
    collected_words = []
    find_event(File_1,event_nr,collected_words)
    find_event(File_2,event_nr,collected_words)
    return collected_words

def strip(raw_data):
    N = len(raw_data)
    fix = []
    for i in range(N):
        s = raw_data[i]
        r1 = s[1:]
        r2 = r1[1:] 		# redundant
        fix.append(r2)
    return fix
                

